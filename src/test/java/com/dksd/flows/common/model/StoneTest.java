package com.dksd.flows.common.model;

import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.factories.FlowCreator;
import com.dksd.flows.common.factories.RuleBuilder;
import com.dksd.flows.common.helper.FilterRulesResult;
import com.dksd.flows.common.helper.RoutingRulesResult;
import com.dksd.flows.common.helper.JsonHelper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StoneTest {

    private Stone stone;
    private UserId userId;
    private Flow flow;

    @BeforeEach
    void setUp() throws FlowNotCreatedException {
        //String partial = "(species == cat AND (color == black OR sex == male))";
        userId = new UserId(UUID.randomUUID());
        flow = new FlowCreator.Builder()
                .withName("Diet Options flow")
                .withUser(userId)
                .withDescription("Diet options flow")
                .beginFunc("select_meals", "test_url", "post")
                .markAsAStartStone()
                .routeIf("first_expression",
                        "next_Step",
                        new RuleBuilder()
                                .with("species", Operator.EQUALS, "cat")
                                .or(new RuleBuilder()
                                        .with("color", Operator.EQUALS, "black")
                                        .with("sex", Operator.EQUALS, "male")
                                        .build())
                                .build())
                .elseIf("else_rule", "else_step",
                        new RuleBuilder().all().build())
                .endFunc().build();
        stone = flow.getStoneByName("select_meals");
    }

    @Test
    void verifyStoneRulesTest() {
        //String partial = "(species == cat AND (color == black OR sex == male))";
        RoutingRulesResult rrs = runAndVerify(stone, true, false,  "species", "cat", "color", "black");
        rrs = runAndVerify(stone, false, true, "species", "dog", "color", "black");
        rrs = runAndVerify(stone, true, false, "species", "cat", "sex", "male");
        rrs = runAndVerify(stone, true, false, "species", "cat", "sex", "female");
        rrs = runAndVerify(stone, false, true, "spes", "cat", "lor", "black");
        assertTrue(rrs.getMatchedIds().contains(stone.getElseRules().getId()));

        ObjectNode o = JsonHelper.createObjectNode();
        o.put("species", "cat");
        o.put("color", "black");
        o.put("sex", "male");
        RoutingRulesResult fr = stone.applyRoutingRules(o);
        assertTrue(fr.getResult());
    }

    private RoutingRulesResult runAndVerify(Stone stone,
                                            boolean expectedResult,
                                            boolean expectedElseResult,
                                            String key,
                                            String val,
                                            String k1,
                                            String val2) {
        ObjectNode o = JsonHelper.createObjectNode();
        o.put(key, val);
        o.put(k1, val2);
        RoutingRulesResult fr = stone.applyRoutingRules(o);
        assertEquals(expectedResult, fr.getResult());
        return fr;
    }

    @Test
    void routeAllFilterTest() throws FlowNotCreatedException {
        Flow flow = new FlowCreator.Builder()
                .withName("Diet Options flow")
                .withUser(userId)
                .withDescription("Diet options flow")
                .beginFunc("select_meals", "test_url", "post")
                .routeAll("next_Step")
                .markAsAStartStone()
                .endFunc().build();
        stone = flow.getStoneByName("select_meals");
        FilterRulesResult fr = stone.applyFilterRules(createSomeData());
        assertTrue(fr.getResult());
    }

    @Test
    void routeAllNextStepTest() throws FlowNotCreatedException {
        Flow flow = new FlowCreator.Builder()
                .withName("Diet Options flow")
                .withUser(userId)
                .withDescription("Diet options flow")
                .beginFunc("select_meals", "test_url", "post")
                .routeAll("next_Step")
                .markAsAStartStone()
                .endFunc().build();
        stone = flow.getStoneByName("select_meals");
        RoutingRulesResult fr = stone.applyRoutingRules(createSomeData());
        assertTrue(fr.getResult());
    }

    private ObjectNode createSomeData() {
        ObjectNode o = JsonHelper.createObjectNode();
        o.put("species", "cat");
        o.put("color", "black");
        return o;
    }
}