package com.dksd.flows.common.model;

import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.factories.FlowCreator;
import com.dksd.flows.common.factories.RuleBuilder;
import com.dksd.flows.common.helper.JsonHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

class FlowTest {

    private Flow flow;

    @BeforeEach
    void setUp() throws FlowNotCreatedException {

        flow = new FlowCreator.Builder()
                .withUser(new UserId(UUID.randomUUID()))
                .withName("Text reply flow")
                .withDescription("Send it some text and it will reply with response options")

                //https://api-inference.huggingface.co/models/
                //https://dzone.com/articles/nested-builder
                .beginFunc("first_text_response", "https://api-inference.huggingface.co/models/facebook/blenderbot-400M-distill", "post")//TEXT CONVERSION

                //TODO this should be here, rather abstracted away
                //that way we can have a pool of option to choose from
                //.addKVPair("url", "huggingface...")
                //.addKVPair("method", "post")
                //.addKVPair("api_key", "key")
                //headers = {"Authorization": f"Bearer {api_token}"}
                //.addKVPair("payload_field", "data")
                .markAsAStartStone()
                .routeIf("first_rule", "second_text_response",
                        new RuleBuilder()
                                .with("generated_text", Operator.CONTAINS, "Robert was a great man")
                                .build())
                .endFunc()

                .beginFunc("second_text_response", "https://localhost:8080/print", "post")
                .endFunc()

                .build();
    }

    @Test
    void serializeTest() throws JsonProcessingException {
        System.out.println(JsonHelper.toJson(flow));
    }

    @Test
    void copyTest() {
    }

    @Test
    void getSignatureTest() {
    }
}