package com.dksd.flows.common.helper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

class ExpiringMapTest {

    private ExpiringMap<String, String> testMap;
    @Mock
    private TimeProvider timeProviderMock;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        testMap = new ExpiringMap<>(100, timeProviderMock);
    }

    @Test
    void getExpireTest() {
        Mockito.when(timeProviderMock.provide()).thenReturn(100L);
        testMap.put("first", "value");
        testMap.put("second", "value");
        testMap.put("third", "value");
        testMap.put("fourth", "value");
        Assertions.assertEquals(testMap.get("first"), "value");
        Mockito.when(timeProviderMock.provide()).thenReturn(101L);
        testMap.get("first");
        Assertions.assertFalse(testMap.isEmpty());
        //Assertions.assertFalse(testMap.contains("first"));
    }
}