package com.dksd.flows.common.factories;

import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.helper.RoutingRulesResult;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.model.Flow;
import com.dksd.flows.common.model.Operator;
import com.dksd.flows.common.model.UserId;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.UUID;

class FlowCreatorTest {

    @BeforeEach
    void setUp() {

    }

    @Test
    public void testComplexRules() throws FlowNotCreatedException {
        Flow flow = new FlowCreator.Builder()
                .withUser(new UserId(UUID.randomUUID()))
                .withName("Diet Options flow")
                .withDescription("Diet options flow")
                .beginFunc("first_text_response", "test_url", "post")
                .markAsAStartStone()
                .routeIf("first_expression",
                        "dest",
                        new RuleBuilder()
                                .with("species", Operator.EQUALS, "cat")
                                .or(new RuleBuilder()
                                        .with("species", Operator.EQUALS, "alien")
                                        .build())
                                .build())

                //This is like an ELSE statement really
                /*.routeElseIf("first_expression")
                .addOrGroup()
                  .addChild("fieldor", "==", "valueor")
                  .addAndGroup()
                    .addChild("fieldand", "==", "valueand")
                  .endGroup()
                .endGroup()
                .then("dest")
                .endElseIf()*/
                //."(species == cat AND (cat != black OR black == blue) AND (cat != black OR black == blue) AND (cat != black OR black == blue) AND (cat != black OR black == blue) AND (cat != black OR black == blue) AND (cat != black OR black == blue))")
                .endFunc()

                .beginFunc("second_text_response", "test_url", "post")
                .endFunc()

                .build();

        ObjectNode on = JsonHelper.createObjectNode();
        on.put("species", "cat");
        on.put("species", "alien");
        RoutingRulesResult fr = flow.getStoneByName("first_text_response").applyRoutingRules(on);
        System.out.println(fr);
    }

    @Test
    public void testElseIdLogicRules() throws FlowNotCreatedException {
        //TODO this is a new feature.
    }


    @Test
    public void testFilterRules() throws FlowNotCreatedException {

    }

    @Test
    public void testRoutingRules() throws FlowNotCreatedException {
        Flow flow = new FlowCreator.Builder()
                .withUser(new UserId(UUID.randomUUID()))
                .withName("Diet Options flow")
                .withDescription("Diet options flow")
                //Maybe instead of enum it should be function meta data.
                .beginFunc("first_text_response", "test_url", "post")
                //.field("text_response").equals("Not your buddy pal")
                //.routeIf().field("text_response").equalsValue("Not your buddy pal.")
                //.then("second_text_response")
                //.endIf()
                //."(species == cat AND (cat != black OR black == blue) AND (cat != black OR black == blue) AND (cat != black OR black == blue) AND (cat != black OR black == blue) AND (cat != black OR black == blue) AND (cat != black OR black == blue))")
                .markAsAStartStone()
                .endFunc()

                .beginFunc("second_text_response", "test_url", "post")
                .endFunc()

                .build();
    }

}