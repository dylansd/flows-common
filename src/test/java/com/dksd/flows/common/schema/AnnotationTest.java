package com.dksd.flows.common.schema;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AnnotationTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void emptyResult() {
    }

    @Test
    void getOriginalFlowMsg() {
    }

    @Test
    void getResults() {
    }

    @Test
    void addSuccessResult() {
    }

    @Test
    void testToString() {
    }

    @Test
    void addError() {
    }
}