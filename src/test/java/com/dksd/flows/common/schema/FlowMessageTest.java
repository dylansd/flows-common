package com.dksd.flows.common.schema;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FlowMessageTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void getId() {
    }

    @Test
    void setId() {
    }

    @Test
    void getUserId() {
    }

    @Test
    void getFlowId() {
    }

    @Test
    void getData() {
    }

    @Test
    void setData() {
    }

    @Test
    void testToString() {
    }

    @Test
    void copyWithDataKeyValues() {
    }

    @Test
    void copyWithoutDataKeyValues() {
    }

    @Test
    void addData() {
    }

    @Test
    void setUserId() {
    }

    @Test
    void setFlowId() {
    }

    @Test
    void getStoneName() {
    }

    @Test
    void setStoneName() {
    }
}