package com.dksd.flows;

import java.util.Arrays;

public final class TestHelper {

    public static boolean compareJsonStrings(String src, String dest) {
        String expectedSorted = sortString(src);
        String destSorted = sortString(dest);
        return expectedSorted.trim().equals(destSorted.trim());
    }

    private static String sortString(String src) {
        char[] expectedSorted = src.toCharArray();
        Arrays.sort(expectedSorted);
        return String.valueOf(expectedSorted);
    }
}
