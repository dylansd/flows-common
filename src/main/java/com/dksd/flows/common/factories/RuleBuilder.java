package com.dksd.flows.common.factories;

import com.dksd.flows.common.model.Gate;
import com.dksd.flows.common.model.Operator;
import com.dksd.flows.common.model.Rule;
import com.dksd.flows.common.model.RuleType;

public class RuleBuilder {

    private Rule rule = new Rule();

    public RuleBuilder() {
        rule.setRuleType(RuleType.AND);
    }

    public Rule build() {
        return rule;
    }

    public RuleBuilder with(String field, Operator op, String value) {
        String type = "string";
        Gate gate = new Gate(field, op, value, type);
        rule.setGate(gate);
        return this;
    }

    public RuleBuilder or(Rule rule) {
        rule.setRuleType(RuleType.OR);
        rule.getChildren().add(rule);
        return this;
    }

    public RuleBuilder and(Rule rule) {
        rule.setRuleType(RuleType.AND);
        rule.getChildren().add(rule);
        return this;
    }

    public RuleBuilder all() {
        rule.setRuleType(RuleType.ALL);
        return this;
    }
}
