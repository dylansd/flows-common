package com.dksd.flows.common.factories;

public interface DataCommonFactory<T> {

    T create(String name);

}
