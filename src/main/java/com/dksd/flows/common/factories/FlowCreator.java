package com.dksd.flows.common.factories;

import com.dksd.flows.common.exceptions.FlowNotCreatedException;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.helper.UUIDProvider;
import com.dksd.flows.common.model.*;
import com.dksd.flows.common.security.SignatureVerification;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Logger;

public final class FlowCreator {

    private static volatile SignatureVerification signatureVerification;
    private final Logger logger = Logger.getLogger(getClass().getName());

    public FlowCreator() {
    }

    public static synchronized SignatureVerification getInstance() {
        if (signatureVerification != null) {
            return signatureVerification;
        }
        try {
            signatureVerification = new SignatureVerification();
            return signatureVerification;
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        throw new IllegalStateException("Could not create SignatureVerification instance");
    }

    public static class FunctionBuilder {
        private final Builder builder;
        private Stone stone;

        public FunctionBuilder(Builder builder) {
            Objects.requireNonNull(builder);
            this.builder = builder;
        }

        public void withFunction(String stoneName, String url, String method) {
            stone = new Stone();
            stone.setName(stoneName);
            stone.setFunctionName(url, method);
            /*stone.setFunctionName(function.getClass().getName());
            stone.setBase64EncodedFunction(serializeFunction(function));
            try {
                stone.setFunctionSignature(FlowCreator.getInstance().sign(
                                stone.getBase64EncodedFunction().getBytes(StandardCharsets.UTF_8)));
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (SignatureException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            functions.put(function.getClass().getName(), function);
            */
            stone.setId(UUIDProvider.provide(stone));
            builder.stoneIds.put(stoneName, stone);
        }

        public FunctionBuilder routeAll(String stoneDestName) {
            Rule rules = new RuleBuilder().build();
            rules.setRuleType(RuleType.ALL);
            rules.setName("route_all");
            rules.setDestStones(Collections.singleton(stoneDestName));
            stone.setRoutingRules(rules);
            return this;
        }

        /*public IfBuilder routeElseIf(String parentIfStatementForElse) {
            IfBuilder ifBuilder = new IfBuilder(this);
            ifBuilder.setFilterRule(false);
            ifBuilder.setName("else_if_for_" + parentIfStatementForElse);
            String parentId = ifBuilder.getIfIdByName(parentIfStatementForElse);
            throw new NotImplementedException("TODO this needs to be implemented aka how to link parent id etc");
        }*/

        /*public FunctionBuilder filterIf(String filterIf) {
            this.rulesHelper.addFilterRule(filterIf, stone.getName());
            return this;
        }*/

        public FunctionBuilder markAsAStartStone() {
            Rule rules = new RuleBuilder().all().build();
            rules.setName("filter_all");
            rules.setDestStones(Collections.singleton(stone.getName()));
            stone.setFilterRules(rules);
            return this;
        }

        public FunctionBuilder withInMsgExample(String msg) {
            stone.addInExampleMessage(msg);
            return this;
        }

        public FunctionBuilder withOutMsgExample(String msg) {
            stone.addOutExampleMessage(msg);
            return this;
        }

        public Builder endFunc() {
            builder.stoneIds.put(stone.getName(), stone);
            return builder;
        }

        public Stone getStone() {
            return stone;
        }

        /*public FunctionBuilder addKVPair(String key, String value) {
            funcKVs.put(key, value);
            return this;
        }*/

        public FunctionBuilder routeIf(String name, String stoneDestName, Rule rules) {
            rules.setName(name);
            rules.setDestStones(Collections.singleton(stoneDestName));
            stone.setRoutingRules(rules);
            return this;
        }

        /**
         * Route if all other routing routes failed to route.
         * @return
         */
        public FunctionBuilder elseIf(String name, String stoneDestName, Rule rules) {
            rules.setName(name);
            rules.setDestStones(Collections.singleton(stoneDestName));
            stone.setElseRules(rules);
            return this;
        }
    }

    public static class Builder {
        private final Map<String, Stone> stoneIds = new HashMap<>();
        private String name;
        private String description;
        private UserId userId;

        public Builder withName(String nameIn) {
            this.name = nameIn;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public FunctionBuilder beginFunc(String stoneName, String url, String method) {
            FunctionBuilder fBuilder = new FunctionBuilder(this);
            fBuilder.withFunction(stoneName, url, method);
            return fBuilder;
        }

        public Flow build() throws FlowNotCreatedException {
            try {
                //TODO do validations here
                // cannot be if without then
                //1. There must be rules
                //2. There must be a finish stone
                //3. Rule validation?
                //4. msg examples?
                //5. msg mappings...

                Flow flow = new Flow();
                flow.setCreatedAt(System.currentTimeMillis());
                flow.setName(name);
                flow.setFlowId(new FlowId(UUIDProvider.provide(name)));
                if (userId == null) {
                    throw new IllegalStateException("User id cannot be null");
                }
                flow.setUserId(userId);
                flow.setDescription(description);
                for (Map.Entry<String, Stone> entry : stoneIds.entrySet()) {
                    flow.addStone(entry.getValue());
                }

                byte[] flowSignature = getInstance().sign(JsonHelper.toJson(flow).getBytes(StandardCharsets.UTF_8));
                flow.setSignature(flowSignature);
                return flow;
            } catch (Exception ep) {
                ep.printStackTrace();
                throw new FlowNotCreatedException("could not create flow: " + name, ep);
            }
        }

        public Builder withUser(UserId id) {
            this.userId = id;
            return this;
        }

        public Builder withUser(User user) {
            this.userId = user.getUserId();
            return this;
        }

    }
}