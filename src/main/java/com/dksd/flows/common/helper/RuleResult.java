package com.dksd.flows.common.helper;

import java.util.*;

public class RuleResult {
    private Set<UUID> matchedIds = new HashSet<>();
    private Map<UUID, String> unmatchedIdsToErrors = new HashMap<>();
    private boolean result;
    private boolean elseResult;
    private Set<String> destStoneNames = new HashSet<>();


    /**
     * Be careful with this method.
     * Could be that 1 rule matches and the overall rule fails.
     * Use only if you know what you are doing.
     *
     * @param id
     * @return
     */
    public boolean addMatched(UUID id) {
        matchedIds.add(id);
        return true;
    }

    public boolean addUnmatched(UUID id, String reason) {
        unmatchedIdsToErrors.put(id, reason);
        return false;
    }

    public boolean getResult() {
        return result;
    }

    public RuleResult setResult(boolean result) {
        this.result = result;
        return this;
    }

    public void setDestStones(Set<String> destStoneIds) {
        this.destStoneNames = destStoneIds;
    }


    public Set<UUID> getMatchedIds() {
        return matchedIds;
    }

    public void setMatchedIds(Set<UUID> matchedIds) {
        this.matchedIds = matchedIds;
    }

    public Map<UUID, String> getUnmatchedIdsToErrors() {
        return unmatchedIdsToErrors;
    }

    public void setUnmatchedIdsToErrors(Map<UUID, String> unmatchedIdsToErrors) {
        this.unmatchedIdsToErrors = unmatchedIdsToErrors;
    }

    public Set<String> getDestStoneNames() {
        return destStoneNames;
    }

    @Override
    public String toString() {
        return "FilterResult{" +
                "matchedIds=" + matchedIds +
                ", unmatchedIds=" + unmatchedIdsToErrors +
                ", result=" + result +
                ", destStoneIds=" + destStoneNames +
                '}';
    }

    public RuleResult mergeElseResult(RuleResult elseRuleResult) {
        this.setElseResult(elseRuleResult.getResult());

        this.matchedIds.addAll(elseRuleResult.getMatchedIds());
        this.unmatchedIdsToErrors.putAll(elseRuleResult.getUnmatchedIdsToErrors());
        this.destStoneNames = elseRuleResult.getDestStoneNames();
        return this;
    }

    private void setElseResult(boolean elseResult) {
        this.elseResult = elseResult;
    }

}
