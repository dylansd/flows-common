package com.dksd.flows.common.helper;

public interface TimeProvider {
    long provide();
}
