package com.dksd.flows.common.helper;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class UUIDProvider {

    public static UUID provideInner(String input) {
        return UUID.nameUUIDFromBytes(input.getBytes(StandardCharsets.UTF_8));
    }

    public static UUID provide(Object... inputs) {
        return provideHelper(inputs);
    }

    public static UUID provideHelper(Object... inputs) {
        HashCodeBuilder hcb = new HashCodeBuilder(17, 31);
        for (Object input : inputs) {
            hcb.append(input);
        }
        String tmp = "" + hcb.toHashCode();
        return provideInner(tmp);
    }

}
