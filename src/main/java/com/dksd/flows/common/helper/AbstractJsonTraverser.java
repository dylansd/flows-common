package com.dksd.flows.common.helper;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class AbstractJsonTraverser {

    private int maxDepth = -1;
    private List<JsonNode> path = new ArrayList<>();

    public AbstractJsonTraverser(JsonNode jsonNode) {
        processNode(jsonNode, 0);
    }

    private void processNode(JsonNode jsonNode, int depth) {
        if (depth > maxDepth) {
            maxDepth = depth;
        }
        if (path.size() <= depth) {
            path.add(jsonNode);
        } else {
            path.set(depth, jsonNode);
        }
        if (jsonNode.isValueNode()) {
            handleValueNode(path.get(depth - 1), jsonNode, depth);
        } else if (jsonNode.isArray()) {
            int i = 0;
            for (JsonNode arrayItem : jsonNode) {
                handleArrayItem(jsonNode, arrayItem, depth, i);
                processNode(arrayItem, depth + 1);
                i++;
            }
        } else if (jsonNode.isObject()) {
            int i = 0;
            for (Iterator<Map.Entry<String, JsonNode>> it = jsonNode.fields(); it.hasNext(); ) {
                Map.Entry<String, JsonNode> entry = it.next();
                handleObject(jsonNode, entry.getKey(), entry.getValue(), depth, i);
                processNode(entry.getValue(), depth + 1);
                i++;
            }
        }
    }

    protected abstract void handleArrayItem(JsonNode parent, JsonNode arrayItem, int depth, int index);

    protected abstract void handleValueNode(JsonNode parent, JsonNode jsonNode, int depth);

    protected abstract void handleObject(JsonNode parent, String key, JsonNode jsonNode, int depth, int index);

}

