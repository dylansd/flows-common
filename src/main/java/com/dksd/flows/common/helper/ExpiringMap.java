package com.dksd.flows.common.helper;

import java.util.Iterator;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ExpiringMap<K, V> extends ConcurrentHashMap<K, V> {

    private final long expiry;
    private final TimeProvider timeProvider;
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
    private TreeMap<Long, Object> expiryMap = new TreeMap<>();
    private TimeUnit timeUnit = TimeUnit.MILLISECONDS;

    public ExpiringMap() {
        this.expiry = 1000 * 60 * 60; //5 minutes
        this.timeProvider = System::currentTimeMillis;
        executorService.scheduleAtFixedRate(this::expireEntries, 10, 10, TimeUnit.SECONDS);
    }

    public ExpiringMap(long expiry, TimeUnit timeUnit) {
        this.expiry = expiry;
        this.timeUnit = timeUnit;
        this.timeProvider = System::currentTimeMillis;
    }

    public ExpiringMap(long expiry, TimeProvider timeProvider) {
        this.expiry = expiry;
        this.timeProvider = timeProvider;
    }

    @Override
    public V put(K key, V value) {
        return super.put(key, value);
    }

    @Override
    public V get(Object key) {
        return super.get(key);
    }

    private void expireEntries() {
        long t = timeProvider.provide();
        int i = 0;
        Iterator<Long> it = expiryMap.headMap(t - expiry).keySet().iterator();
        while (it.hasNext() && i < 100) {
            long k = it.next();
            Object ret = expiryMap.get(k);
            if (ret != null) {
                remove(ret);
            }
            it.remove();
            i++;
        }
    }
}
