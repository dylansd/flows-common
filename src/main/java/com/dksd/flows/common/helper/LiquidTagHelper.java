package com.dksd.flows.common.helper;

import com.fasterxml.jackson.databind.JsonNode;

public final class LiquidTagHelper {

    static String stb = "{{";
    static String eb = "}}";

    public static String getArrayNode(JsonNode data, String str) {
        String finalStr = "";
        int indx = str.indexOf(stb);
        int eindx = str.indexOf(eb);
        int i = indx;
        while (i >= 0) {
            String field = "";
            while (i < eindx) {
                field += str.charAt(i);
                i++;
            }
            String dFromJson = data.get(field).asText();
            //subsitute the variable here. or add to map
            indx = str.indexOf(stb);
            eindx = str.indexOf(eb);
            i = indx;
        }
        return finalStr;
    }

}
