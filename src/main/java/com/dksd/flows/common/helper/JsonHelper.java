package com.dksd.flows.common.helper;

import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;
import java.io.IOException;

public final class JsonHelper {

    private static final ObjectMapper objectMapper = new ObjectMapper()
            .enable(SerializationFeature.INDENT_OUTPUT);

    public static String createJson(String key, String value) {
        return createWithKeyValue(key, value).toPrettyString();
    }

    public static String createJson(String key, String value, String key2, String value2) {
        return createWithKeyValue(key, value).put(key2, value2).toPrettyString();
    }

    public static ObjectNode createWithKeyValue(String key, String value) {
        return createObjectNode().put(key, value);
    }

    public static ObjectNode createWithKeyValue(String key, String value, String key2, String value2) {
        ObjectNode on = createObjectNode().put(key, value);
        on.put(key2, value2);
        return on;
    }

    public static ObjectNode createObjectNode() {
        return objectMapper.createObjectNode();
    }

    public static ObjectMapper getObjectMapper() {
        return objectMapper;
    }

    public static JsonNode getParsedPayload(FlowMsgData flowMsg) throws JsonProcessingException {
        return parseJson(flowMsg.getData());
    }

    public static ObjectNode getParsedPayloadObj(FlowMsgData flowMsg) throws JsonProcessingException {
        return getParsedPayload(flowMsg).deepCopy();
    }

    public static <T> T parseToObj(String msg, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(msg, clazz);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JsonNode parseJson(File file) throws IOException {
        return getObjectMapper().readTree(file);
    }

    public static JsonNode parseJson(String json) throws JsonProcessingException {
        return getObjectMapper().readTree(json);
    }

    public static String toJson(Object node) throws JsonProcessingException {
        return getObjectMapper().writeValueAsString(node);
    }

    public static String toJsonNoEx(Object node) {
        try {
            return getObjectMapper().writeValueAsString(node);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayNode createArrayNode() {
        return getObjectMapper().createArrayNode();
    }

    public static ArrayNode createDoubleArrayNode(double[] arr) {
        ArrayNode inputArr = JsonHelper.createArrayNode();
        for (int i = 0; i < arr.length; i++) {
            inputArr.add(arr[i]);
        }
        return inputArr;
    }

    public static ArrayNode getArrayNode(JsonNode json, String field) {
        if (json.get(field) == null) {
            return null;
        }
        if (!json.get(field).isArray()) {
            return JsonHelper.createArrayNode();
        }
        return (ArrayNode) json.get(field);
    }

}
