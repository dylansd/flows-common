package com.dksd.flows.common.exceptions;

public final class FlowNotFoundException extends Exception {

    public FlowNotFoundException(String msg) {
        super(msg);
    }

    public FlowNotFoundException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
