package com.dksd.flows.common.exceptions;

public class FlowNotCreatedException extends Exception {

    public FlowNotCreatedException(String msg) {
        super(msg);
    }

    public FlowNotCreatedException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
