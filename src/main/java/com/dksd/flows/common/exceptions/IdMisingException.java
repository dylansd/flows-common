package com.dksd.flows.common.exceptions;

public final class IdMisingException extends Throwable {
    public IdMisingException(String msg) {
        super(msg);
    }
}
