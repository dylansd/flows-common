package com.dksd.flows.common.exceptions;

public final class UserNotAllowedException extends Exception {

    public UserNotAllowedException(String message) {
        super(message);
    }

    public UserNotAllowedException(String message, Throwable cause) {
        super(message, cause);
    }

}
