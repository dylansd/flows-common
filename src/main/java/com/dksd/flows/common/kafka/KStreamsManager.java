package com.dksd.flows.common.kafka;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

public class KStreamsManager {

    private Map<String, KafkaStreams> streams = new ConcurrentHashMap<>();

    public KStreamsManager() {
        Runtime.getRuntime().addShutdownHook(new Thread("shutdown-hook") {
            @Override
            public void run() {
                System.out.println("Shutdown streams...");
                for (Map.Entry<String, KafkaStreams> entry : streams.entrySet()) {
                    stopStream(entry.getKey());
                }
            }
        });
    }

    public void runStream(String streamName,
                          KafkaStreams stream) {
        try {
            streams.put(streamName, stream);
            System.out.println("Streams started...");
            stream.start();
        } catch (final Throwable e) {
            System.err.println(e.getMessage());
            //Add error handling send to kafka metrics etc.
        }
    }

    public void stopStream(String streamName) {
        streams.get(streamName).close();
    }

    public Properties getStreamTemplateProperties() {
        Properties props = new Properties();
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        return props;
    }

}
