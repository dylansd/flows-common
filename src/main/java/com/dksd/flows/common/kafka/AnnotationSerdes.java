package com.dksd.flows.common.kafka;

import com.dksd.flows.common.schema.Annotation;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

public final class AnnotationSerdes {

    public static Serde<Annotation> instance() {
        return new AnnotationSerde();
    }

    static public final class AnnotationSerde extends Serdes.WrapperSerde<Annotation> {

        public AnnotationSerde() {
            super(new JsonSerializer<>(),
                    new JsonDeserializer<>(Annotation.class));
        }
    }

}
