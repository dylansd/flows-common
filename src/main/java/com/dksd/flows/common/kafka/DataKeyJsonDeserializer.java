package com.dksd.flows.common.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

public class DataKeyJsonDeserializer implements Deserializer<DataKey> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    public DataKeyJsonDeserializer() {
    }

    @Override
    public DataKey deserialize(String topic, byte[] bytes) {
        if (bytes == null) {
            return null;
        }

        DataKey data;
        try {
            data = objectMapper.readValue(bytes, DataKey.class);
        } catch (Exception e) {
            throw new SerializationException(e);
        }

        return data;
    }

}
