package com.dksd.flows.common.kafka.consumers;

import com.dksd.flows.common.kafka.TopicDefs;
import com.dksd.flows.common.model.FlowInfo;
import com.dksd.flows.common.model.Tracker;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TrackerConsumer {

    public static final String INPUT_TOPIC = TopicDefs.TRACKER_TOPIC;
    private final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(getClass().getName());
    private final FlowInfo flowInfo;
    private Consumer<UUID, Tracker> consumer;
    private ExecutorService executors = Executors.newSingleThreadExecutor();

    public TrackerConsumer(KafkaConsumer<UUID, Tracker> consumer, FlowInfo flowInfo) {
        this.consumer = consumer;
        this.flowInfo = flowInfo;
        consumer.subscribe(Arrays.asList(INPUT_TOPIC), new ConsumerRebalanceListener() {
            @Override
            public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
                for (TopicPartition partition : partitions) {
                    logger.info("Partitions Revoked: " + partition.partition());
                }
            }

            @Override
            public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
                for (TopicPartition partition : partitions) {
                    logger.info("Partitions assigned: " + partition.partition());
                }
                consumer.seekToBeginning(consumer.assignment());
                logger.info("Tracker consumer seeking to beginning!");
            }
        });
        executors.submit(this::consumeMessages);
    }

    public void consumeMessages() {
        try {
            while (true) {
                try {
                    ConsumerRecords<UUID, Tracker> consumerRecords =
                            consumer.poll(Duration.of(1000, ChronoUnit.MILLIS));

                    for (ConsumerRecord<UUID, Tracker> record : consumerRecords) {
                        updateTrackerInfo(record);
                        //logger.info("Received tracker update: " + record.key() + ", " + record.value());
                    }
                } catch (Exception ep) {
//                logger.severe("Could not process tracker message, offset: " + );
                    ep.printStackTrace();
                } finally {
                    consumer.commitSync();
                }
            }
        } catch (Throwable t) {
            //Address not found exception if not connected...
            logger.severe("Could not complete tracker poll: " + t.getMessage());
        }
    }

    private void updateTrackerInfo(ConsumerRecord<UUID, Tracker> record) {
        flowInfo.update(record.value());
    }

}

