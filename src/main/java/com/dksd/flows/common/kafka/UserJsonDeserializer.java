package com.dksd.flows.common.kafka;

import com.dksd.flows.common.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

public class UserJsonDeserializer implements Deserializer<User> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    public UserJsonDeserializer() {
    }

    @Override
    public User deserialize(String topic, byte[] bytes) {
        if (bytes == null) {
            return null;
        }

        User data;
        try {
            data = objectMapper.readValue(bytes, User.class);
        } catch (Exception e) {
            throw new SerializationException(e);
        }

        return data;
    }

}
