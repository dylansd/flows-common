package com.dksd.flows.common.kafka;

import com.dksd.flows.common.schema.FlowMsgData;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

public final class MsgDataSerdes {

    public static Serde<FlowMsgData> instance() {
        return new FlowMsgdataSerde();
    }

    static public final class FlowMsgdataSerde
            extends Serdes.WrapperSerde<FlowMsgData> {
        public FlowMsgdataSerde() {
            super(new JsonSerializer<>(),
                    new JsonDeserializer<>(FlowMsgData.class));
        }
    }

}
