package com.dksd.flows.common.kafka;

import com.dksd.flows.common.model.User;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

public final class UserSerdes {

    public static Serde<User> instance() {
        return new UserSerde();
    }

    static public final class UserSerde extends Serdes.WrapperSerde<User> {

        public UserSerde() {
            super(new JsonSerializer<>(),
                    new JsonDeserializer<>(User.class));
        }
    }

}
