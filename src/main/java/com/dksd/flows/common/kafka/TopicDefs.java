package com.dksd.flows.common.kafka;

public class TopicDefs {

    ///LATER Maight want to add a to User topic
    //and a from user topic, to be able to translate the stuff correctly.
    //USer flows ?
    public static final String FLOW_TOPIC = "flows";
    public static final String ANNO_TOPIC = "annotations";
    public static final String USERS_TOPIC = "users";
    public static final String FLOWMSGDATA_TOPIC = "flowmsgdata";
    public static final String TRACKER_TOPIC = "tracker";
    public static final String PROJECTS_TOPIC = "projects";
    //Pump EVERYTHING through here, if unsure pump it through
    public static final String METRICS_TOPIC = "eai.metrics";
    // Pump all data through here
    public static final String DATA_TOPIC = "data";
    public static final String DEAD_LETTERS_TOPIC = "eai.deadletters";
    public static final String USER_ERRORS_TOPIC = "eai.user.errors";

}
