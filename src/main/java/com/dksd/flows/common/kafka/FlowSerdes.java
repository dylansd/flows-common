package com.dksd.flows.common.kafka;

import com.dksd.flows.common.model.Flow;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;

public final class FlowSerdes {

    public static Serde<Flow> instance() {
        return new FlowSerde();
    }

    static public final class FlowSerde extends Serdes.WrapperSerde<Flow> {

        public FlowSerde() {
            super(new JsonSerializer<>(),
                    new JsonDeserializer<>(Flow.class));
        }
    }

}
