package com.dksd.flows.common.kafka;

import com.dksd.flows.common.model.Flow;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;

public class FlowJsonDeserializer implements Deserializer<Flow> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    public FlowJsonDeserializer() {
    }

    @Override
    public Flow deserialize(String topic, byte[] bytes) {
        if (bytes == null) {
            return null;
        }

        Flow data = null;
        try {
            data = objectMapper.readValue(bytes, Flow.class);
        } catch (Exception e) {
            //throw new SerializationException(e);
        }

        return data;
    }

}
