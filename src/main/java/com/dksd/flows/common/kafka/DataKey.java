package com.dksd.flows.common.kafka;

import com.dksd.flows.common.model.FlowId;
import com.dksd.flows.common.model.UserId;

import java.util.StringJoiner;
import java.util.UUID;

public class DataKey {
    private String key;
    private String storeName;
    private UUID dataStoreId;
    private UserId userId;
    private FlowId flowId;

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    public FlowId getFlowId() {
        return flowId;
    }

    public void setFlowId(FlowId flowId) {
        this.flowId = flowId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DataKey.class.getSimpleName() + "[", "]")
                .add("key='" + key + "'")
                .add("storeName='" + storeName + "'")
                .add("userId=" + userId)
                .add("flowId=" + flowId)
                .toString();
    }

    public UUID getDataStoreId() {
        return dataStoreId;
    }

    public void setDataStoreId(UUID dataStoreId) {
        this.dataStoreId = dataStoreId;
    }
}
