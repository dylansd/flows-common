package com.dksd.flows.common.kafka;

import com.dksd.flows.common.model.Tracker;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

public class TrackerJsonDeserializer implements Deserializer<Tracker> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    public TrackerJsonDeserializer() {
    }

    @Override
    public Tracker deserialize(String topic, byte[] bytes) {
        if (bytes == null) {
            return null;
        }

        Tracker data;
        try {
            data = objectMapper.readValue(bytes, Tracker.class);
        } catch (Exception e) {
            throw new SerializationException(e);
        }

        return data;
    }

}
