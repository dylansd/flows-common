package com.dksd.flows.common.client;

import java.io.Serializable;

public class DataObj implements Serializable {

    private String id;
    private String data;
    private DataUploadObj o;

    public DataObj() {

    }

    public DataObj(String id) {
        this.id = id;
    }

    public DataObj(String id, String data) {
        this.id = id;
        this.data = data;
    }

    public DataObj(DataUploadObj obj) {
        o = obj;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DataUploadObj getO() {
        return o;
    }

    public void setO(DataUploadObj o) {
        this.o = o;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
