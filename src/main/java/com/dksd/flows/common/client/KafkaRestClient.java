package com.dksd.flows.common.client;

import com.fasterxml.jackson.databind.JsonNode;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;
import java.util.UUID;

public interface KafkaRestClient {

    @POST("/consumers/{consumer_id}")
    Call<JsonNode> registerConsumer(
            @Path("consumer_id") String consumerId,
            @Query("user_id") UUID userId,
            @Body JsonNode consumers);

    @GET("/consumers/{consumer_id}/records")
    Call<JsonNode> getRecords(
            @Query("user_id") UUID userId);

    @POST("/topics/records")
    Call<List<JsonNode>> putRecords(
            @Query("user_id") UUID userId,
            @Body List<JsonNode> records);

}
