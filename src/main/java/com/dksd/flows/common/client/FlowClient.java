package com.dksd.flows.common.client;

import com.dksd.flows.common.model.Flow;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.UUID;

public interface FlowClient {

    @GET("/flow/{flow_id}")
    Call<Flow> getFlow(@Path("flow_id") UUID flowId,
                       @Query("user_id") UUID userId);

    @PUT("/flow")
    Call<Flow> putFlow(@Query("user_id") UUID userId,
                       @Body Flow flow);

}
