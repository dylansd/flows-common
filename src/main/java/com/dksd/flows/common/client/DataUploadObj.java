package com.dksd.flows.common.client;

import java.io.Serializable;
import java.util.UUID;

public final class DataUploadObj implements Serializable {
    private String fileName;
    private String objId;
    private UUID userId;
    private UUID flowId;
    private String apiKey;
    private String data;
    private boolean pathRelativeToFlow = true;

    //String encPath = new String(Base64.getEncoder().encode(path.getBytes(StandardCharsets.UTF_8)));
    public DataUploadObj() {

    }

    public DataUploadObj(
            String fileName,
            String objId,
            UUID userId,
            UUID flowId,
            String apiKey,
            String data) {
        this.fileName = fileName;
        this.objId = objId;
        this.userId = userId;
        this.flowId = flowId;
        this.apiKey = apiKey;
        this.data = data;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getObjId() {
        return objId;
    }

    public void setObjId(String objId) {
        this.objId = objId;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public UUID getFlowId() {
        return flowId;
    }

    public void setFlowId(UUID flowId) {
        this.flowId = flowId;
    }

    public boolean isPathRelativeToFlow() {
        return pathRelativeToFlow;
    }

    public void setPathRelativeToFlow(boolean pathRelativeToFlow) {
        this.pathRelativeToFlow = pathRelativeToFlow;
    }

}
