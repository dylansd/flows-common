package com.dksd.flows.common.client;

import com.dksd.flows.common.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Path;

import java.util.UUID;

public interface UserClient {

    @GET("/user/{user_id}")
    Call<User> getUser(@Path("user_id") UUID userId);

    @PUT("/user")
    Call<User> putUser(@Body User user);

}
