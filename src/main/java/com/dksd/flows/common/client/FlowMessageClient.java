package com.dksd.flows.common.client;

import com.dksd.flows.common.model.FlowId;
import com.dksd.flows.common.model.UserId;
import com.dksd.flows.common.schema.FlowMsgData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface FlowMessageClient {

    @GET("/flow_message")
    Call<List<FlowMsgData>> getFlowMessages(@Query("user_id") UUID userId,
                                            @Query("flow_id") UUID flowId);

    @PUT("/flow_message")
    Call<List<FlowMsgData>> putFlowMessages(@Query("user_id") UserId userId,
                                            @Query("flow_id") FlowId flowId,
                                            @Body Collection<FlowMsgData> flowMessages);

}
