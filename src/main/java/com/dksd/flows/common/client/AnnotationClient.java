package com.dksd.flows.common.client;

import com.dksd.flows.common.model.FlowId;
import com.dksd.flows.common.model.UserId;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.PUT;
import retrofit2.http.Query;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface AnnotationClient {

    @PUT("/annotations")
    Call<List<FlowMsgData>> putAnnotations(
            @Query("user_id") UserId userId,
            @Query("flow_id") FlowId flowId,
            @Query("orig_msg_id") UUID origMsgId,
            @Body Collection<Annotation> annotations);

}
