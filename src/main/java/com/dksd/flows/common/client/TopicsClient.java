package com.dksd.flows.common.client;

import com.dksd.flows.common.model.FileItem;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;
import java.util.UUID;

/**
 * Maybe this is too big a scope.
 * What about this idea:
 * flow data,
 * function data,
 * Global shared data
 */
public interface TopicsClient {

    @PUT("/topics/{topic_name}")
    Call<DataObj> putObject(@Body DataUploadObj dataUploadObj);

    @GET("/data/object/{filename}")
    Call<DataObj> getObject(@Path("filename") String filename,
                            @Query("user_id") UUID userId,
                            @Query("flow_id") UUID flowId);

  /*@GET("/data/object/{obj_id}")
  Call<DataObj> getJsonById(
          @Path("path") String path,
          @Path("obj_id") String objId,
          @Query("user_id") UUID userId,
          @Header("authorization") String apiKey);*/

    @GET("/data/object/{path}")
    Call<List<FileItem>> listFiles(
            @Query("path") String path,
            @Query("user_id") UUID userId);

  /*@Streaming
  @GET
  Call<ResponseBody> downloadFileWithDynamicUrlAsync(@Query("user_id") UUID userId,
                                                     @Header("authorization") String apiKey,
                                                     @Query("path") String path,
                                                     @Url String fileUrl);*/

}