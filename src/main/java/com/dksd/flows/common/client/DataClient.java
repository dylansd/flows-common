package com.dksd.flows.common.client;

import com.dksd.flows.common.model.FileItem;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;
import java.util.UUID;

/**
 * Maybe this is too big a scope.
 * What about this idea:
 * flow data,
 * function data,
 * Global shared data
 */
public interface DataClient {

    @PUT("/data/object")
    Call<DataObj> putObject(@Body DataUploadObj dataUploadObj);

  /*@Multipart
  @POST("/data/upload_file")
  Call<DataObj> uploadFile(@Part MultipartBody.Part file,
                           @Part("file") RequestBody name);*/

    @Multipart
    @POST("/data/upload_file")
    Call<DataObj> uploadFile(@Part("file\"; filename=\"pp.png\" ") RequestBody file,
                             @Part("filename") RequestBody filename);

    @GET("/data/object/{filename}")
    Call<DataObj> getObject(@Path("filename") String filename,
                            @Query("user_id") UUID userId,
                            @Query("flow_id") UUID flowId);


    @GET("/data/object/{path}")
    Call<List<FileItem>> listFiles(
            @Query("path") String path,
            @Query("user_id") UUID userId);

  /*@Streaming
  @GET
  Call<ResponseBody> downloadFileWithDynamicUrlAsync(@Query("user_id") UUID userId,
                                                     @Header("authorization") String apiKey,
                                                     @Query("path") String path,
                                                     @Url String fileUrl);*/

}