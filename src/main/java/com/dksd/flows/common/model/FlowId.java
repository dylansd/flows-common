package com.dksd.flows.common.model;

import org.jetbrains.annotations.NotNull;
import org.teavm.flavour.json.JsonPersistable;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@JsonPersistable
public final class FlowId implements Serializable, Comparable<FlowId> {
    private UUID flowId;

    public FlowId() {
    }

    public FlowId(UUID flowId) {
        this.flowId = flowId;
    }

    public FlowId(String flowId) {
        this.flowId = UUID.fromString(flowId);
    }

    @Override
    public String toString() {
        return flowId.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FlowId flowId1 = (FlowId) o;
        return Objects.equals(flowId, flowId1.flowId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flowId);
    }

    public UUID getFlowId() {
        return flowId;
    }

    public void setFlowId(UUID flowId) {
        this.flowId = flowId;
    }


    @Override
    public int compareTo(@NotNull FlowId o) {
        return o.getFlowId().compareTo(flowId);
    }
}
