package com.dksd.flows.common.model;

public enum FlowState {
    RUNNING, PAUSED
}
