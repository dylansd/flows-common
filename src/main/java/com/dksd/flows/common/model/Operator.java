package com.dksd.flows.common.model;

import java.io.Serializable;

public enum Operator implements Serializable {
    EQUALS, NOT_EQUALS, LT, GT, LTE, GTE, CONTAINS

}
