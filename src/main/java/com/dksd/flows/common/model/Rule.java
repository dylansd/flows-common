package com.dksd.flows.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Rule implements Serializable {
    private final UUID id;
    private RuleType ruleType;
    //If this is null and there are children then is
    // the top level rule/route
    private String name;
    private Gate gate;
    private Set<Rule> children = new HashSet<>();
    private Set<String> destStones = new HashSet<>();
    private Set<String> elseStones = new HashSet<>();

    public Rule() {
        id = UUID.randomUUID();
    }

    public UUID getId() {
        return id;
    }

    public RuleType getRuleType() {
        return ruleType;
    }

    public void setRuleType(RuleType ruleType) {
        this.ruleType = ruleType;
    }

    public Gate getGate() {
        return gate;
    }

    public void setGate(Gate gate) {
        this.gate = gate;
    }

    public Set<Rule> getChildren() {
        return children;
    }

    public void setChildren(Set<Rule> children) {
        this.children = children;
    }

    public Set<String> getDestStones() {
        return destStones;
    }

    public void setDestStones(Set<String> destStones) {
        this.destStones = destStones;
    }

    public Set<String> getElseStones() {
        return elseStones;
    }

    public void setElseStones(Set<String> elseStones) {
        this.elseStones = elseStones;
    }

    @JsonIgnore
    public boolean hasChildren() {
        return children != null && !children.isEmpty();
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public boolean isAll() {
        return RuleType.ALL.equals(ruleType);
    }

    public String getName() {
        return name;
    }

}