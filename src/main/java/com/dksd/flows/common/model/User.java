package com.dksd.flows.common.model;

import com.dksd.flows.common.helper.UUIDProvider;
import org.apache.commons.lang3.RandomStringUtils;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Id;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;
import org.teavm.flavour.json.JsonPersistable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

@Indices({
  @Index(value = "apiKey", type = IndexType.Unique),
  @Index(value = "name", type = IndexType.NonUnique)
})
@JsonPersistable
public final class User {
    private String name;
    @Id
    private UserId userId;
    private String apiKey;
    private long cashPaidInCents;//total cash the user has paid so far
    private List<String> errorMsgs = new ArrayList<>();

    public User() {
    }

    public User(UserId userId, String name) {
        this.userId = userId;
        this.name = name;
        this.apiKey = RandomStringUtils.randomAlphabetic(10);
    }

    public User(String name) {
        this(new UserId(UUIDProvider.provide(name)), name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public void setId(UserId userId) {
        this.userId = userId;
    }

    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(userId, user.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("userId=" + userId)
                .add("apiKey='" + apiKey + "'")
                .add("errors='" + errorMsgs + "'")
                .toString();
    }

    public long getCashPaidInCents() {
        return cashPaidInCents;
    }

    public void setCashPaidInCents(long cashPaidInCents) {
        this.cashPaidInCents = cashPaidInCents;
    }

    public List<String> getErrorMsgs() {
        return errorMsgs;
    }

    public void setErrorMsgs(List<String> errorMsgs) {
        this.errorMsgs = errorMsgs;
    }

    public void addErrorMsg(String errorMsg) {
        this.errorMsgs.add(errorMsg);
    }

}
