package com.dksd.flows.common.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class Tracker {
    private UUID id;
    private UserId userId;
    private FlowId flowId;
    private UUID srcStone;
    private UUID destStone;
    private UUID parentId;
    private UUID sessionId;
    private UUID runId;
    private MsgType msgType;
    private CheckPoint checkPoint;
    //This is a comma seperated list of ids that match the action field above
    private Set<String> filterRoutingRuleIds;
    private int payloadSizeBytes;
    private long createdAt;
    private Exception exception;

    public Tracker() {
        createdAt = System.currentTimeMillis();
    }

    public Tracker(UUID id) {
        this.id = id;
        this.createdAt = System.currentTimeMillis();
    }

    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
    }

    public FlowId getFlowId() {
        return flowId;
    }

    public void setFlowId(FlowId flowId) {
        this.flowId = flowId;
    }

    public UUID getSrcStone() {
        return srcStone;
    }

    public void setSrcStone(UUID srcStone) {
        this.srcStone = srcStone;
    }

    public UUID getDestStone() {
        return destStone;
    }

    public void setDestStone(UUID destStone) {
        this.destStone = destStone;
    }

    public UUID getParentId() {
        return parentId;
    }

    public Tracker setParentId(UUID parentId) {
        this.parentId = parentId;
        return this;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public MsgType getMsgType() {
        return msgType;
    }

    public void setMsgType(MsgType msgType) {
        this.msgType = msgType;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getSessionId() {
        return sessionId;
    }

    public void setSessionId(UUID sessionId) {
        this.sessionId = sessionId;
    }

    public UUID getRunId() {
        return runId;
    }

    public void setRunId(UUID runId) {
        this.runId = runId;
    }

    public int getPayloadSizeBytes() {
        return payloadSizeBytes;
    }

    public void setPayloadSizeBytes(int payloadSizeBytes) {
        this.payloadSizeBytes = payloadSizeBytes;
    }

    public Set<String> getFilterRoutingRuleIds() {
        return filterRoutingRuleIds;
    }

    public void setFilterRoutingRuleIds(Set<String> filterRoutingRuleIds) {
        this.filterRoutingRuleIds = filterRoutingRuleIds;
    }

    public CheckPoint getCheckPoint() {
        return checkPoint;
    }

    public void setCheckPoint(CheckPoint checkPoint) {
        this.checkPoint = checkPoint;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tracker tracker = (Tracker) o;
        return payloadSizeBytes == tracker.payloadSizeBytes && createdAt == tracker.createdAt && Objects.equals(id, tracker.id) && Objects.equals(userId, tracker.userId) && Objects.equals(flowId, tracker.flowId) && Objects.equals(srcStone, tracker.srcStone) && Objects.equals(destStone, tracker.destStone) && Objects.equals(parentId, tracker.parentId) && Objects.equals(sessionId, tracker.sessionId) && Objects.equals(runId, tracker.runId) && msgType == tracker.msgType && checkPoint == tracker.checkPoint && Objects.equals(filterRoutingRuleIds, tracker.filterRoutingRuleIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, flowId, srcStone, destStone, parentId, sessionId, runId, msgType, checkPoint, filterRoutingRuleIds, payloadSizeBytes, createdAt);
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "Tracker{" +
                    "msgId=" + id +
                    "userId=" + userId +
                    ", flowId=" + flowId +
                    ", srcStone=" + srcStone +
                    ", destStone=" + destStone +
                    ", parentId=" + parentId +
                    ", msgType=" + msgType +
                    ", updatedAt=" + createdAt +
                    '}';
        }
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
