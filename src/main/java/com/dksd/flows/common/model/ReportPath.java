package com.dksd.flows.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.TreeMap;
import java.util.UUID;

/**
 * Holds a path for a specific id through the workflow.
 */
public class ReportPath {

    private UUID rootMsgId;
    private TreeMap<Long, Tracker> path = new TreeMap<>();

    public ReportPath() {
    }

    public ReportPath(UUID rootMsgId) {
        this.rootMsgId = rootMsgId;
    }

    public UUID getRootMsgId() {
        return rootMsgId;
    }

    public void add(Tracker tracker) {
        path.put(tracker.getCreatedAt(), tracker);
    }

    @JsonIgnore
    public Tracker getRootTracker() throws IllegalStateException {
        for (Tracker entry : path.values()) {
            if (entry.getId().equals(rootMsgId)) {
                return entry;
            }
        }
        throw new IllegalStateException("Root not found!");
    }

    public TreeMap<Long, Tracker> getPath() {
        return this.path;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "ReportPath{" +
                    "rootMsgId=" + rootMsgId +
                    ", path=" + path +
                    '}';
        }
    }
}
