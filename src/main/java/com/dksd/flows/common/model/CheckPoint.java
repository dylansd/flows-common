package com.dksd.flows.common.model;

public enum CheckPoint {
    FUNC_CREATE_ANNO_OUT_ERROR,
    FUNC_CREATE_ANNO_OUT,
    FLOWMSG_FILTER_UNMATCHED_EXEC,
    FLOWMSG_FILTER_MATCHED_EXEC,
    ANNO_IN_CONTROLLER,
    FLOWMSG_IN_CONTROLLER,
    ANNO_ROUTING_UNMATCHED_EXEC,
    ANNO_ROUTING_MATCHED_EXEC
}
