package com.dksd.flows.common.model;

import com.dksd.flows.common.helper.UUIDProvider;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.teavm.flavour.json.JsonPersistable;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Logger;

@JsonPersistable
public final class Gate implements Serializable {
    @JsonIgnore
    private final Logger logger = Logger.getLogger(getClass().getName());
    private UUID id;
    private String field;
    private Operator operator;
    private String value;
    private String valueType;
    private int valueInt;
    private double valueDbl;

    public Gate() {
        //NOOP
    }

    public Gate(String field,
                Operator operator,
                String value,
                String valueType) {
        this.id = UUIDProvider.provide("" + (field + operator + value + valueType).hashCode());
        this.field = field;
        this.operator = operator;
        this.value = value;
        this.valueType = valueType;
        if ("int".equals(valueType)) {
            valueInt = Integer.parseInt(value);
        }
        if ("double".equals(valueType)) {
            valueDbl = Double.parseDouble(value);
        }
    }

    public boolean filter(JsonNode data, Set<String> errors) {
        try {
            if (data.get(field) == null && "boolean".equals(valueType) && "false".equals(value)) {
                return true;
            }
            if (data.get(field) == null) {
                String err = "Field: " + field + " is not present! Rules cannot be applied or evaluated! Data: " + new ObjectMapper().writeValueAsString(data);
                errors.add(err);
                logger.severe(err);
                return false;
            }
            switch (valueType) {
                case "int":
                    if (data.get(field).isInt()) {
                        return eval(data.get(field).asInt(), valueInt);
                    }
                    return eval(Integer.parseInt(data.get(field).asText()), Integer.parseInt(value));
                case "double":
                    if (data.get(field).isDouble()) {
                        return eval(data.get(field).asDouble(), valueDbl);
                    }
                    return eval(Double.parseDouble(data.get(field).asText()), Double.parseDouble(value));
                case "string":
                    return eval(data.get(field).asText(), value);
                case "boolean":
                    return "true".equals(data.get(field).asText());
                default:
                    return false;
            }
        } catch (Exception ep) {
            String msg = "Error filtering data: " + data.toPrettyString() + " exception: " + ep;
            errors.add(msg);
            logger.severe(msg);
        }
        return false;
    }

    public String getField() {
        return field;
    }

    public Operator getOperator() {
        return operator;
    }

    public String getValue() {
        return value;
    }

    public String getValueType() {
        return valueType;
    }

    public UUID getId() {
        return id;
    }

  /*
  private final String field;
  private final Operator operator;
  private final String value;
  private final CompareType valueType;
   */

    private boolean eval(int d1, int d2) {
        switch (operator) {
            case EQUALS:
                return d1 == d2;
            case NOT_EQUALS:
                return d1 != d2;
            case LT:
                return d1 < d2;
            case LTE:
                return d1 <= d2;
            case GT:
                return d1 > d2;
            case GTE:
                return d1 >= d2;
            default:
                return false;
        }
    }

    private boolean eval(String d1, String d2) {
        switch (operator) {
            case CONTAINS:
                return d1.contains(d2) || d2.contains(d1);
            case EQUALS:
                return d1.equals(d2);
            case NOT_EQUALS:
                return !d1.equals(d2);
            case LT:
                return d1.compareTo(d2) < 0;
            case LTE:
                return d1.compareTo(d2) <= 0;
            case GT:
                return d1.compareTo(d2) > 0;
            case GTE:
                return d1.compareTo(d2) >= 0;
            default:
                return false;
        }
    }

    private boolean eval(double d1, double d2) {
        switch (operator) {
            case EQUALS:
                return d1 == d2;
            case NOT_EQUALS:
                return d1 != d2;
            case LT:
                return d1 < d2;
            case LTE:
                return d1 <= d2;
            case GT:
                return d1 > d2;
            case GTE:
                return d1 >= d2;
            default:
                return false;
        }
    }

}
