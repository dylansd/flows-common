package com.dksd.flows.common.model;

public enum MsgType {
    ANNOTATION, MSGDATA, ERROR
}
