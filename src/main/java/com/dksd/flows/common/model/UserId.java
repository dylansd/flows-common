package com.dksd.flows.common.model;

import org.jetbrains.annotations.NotNull;
import org.teavm.flavour.json.JsonPersistable;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;
import java.util.UUID;

@JsonPersistable
public final class UserId implements Serializable, Comparable<UserId> {
    private UUID userId;
    private String shortId;

    public UserId() {
    }

    public UserId(UUID userId) {
        this.userId = userId;
    }

    public UserId(String userId) {
        this.userId = UUID.fromString(userId);
    }

    @Override
    public String toString() {
        return userId.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserId userId1 = (UserId) o;
        return Objects.equals(userId, userId1.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }


    @Override
    public int compareTo(@NotNull UserId o) {
        return o.getUserId().compareTo(userId);
    }
}
