package com.dksd.flows.common.model;

import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.helper.UUIDProvider;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.dizitart.no2.IndexType;
import org.dizitart.no2.objects.Index;
import org.dizitart.no2.objects.Indices;
import org.teavm.flavour.json.JsonPersistable;

import java.util.*;
import java.util.logging.Logger;

//@Entity
@Indices({
  @Index(value = "flowId", type = IndexType.Unique),
  @Index(value = "userId", type = IndexType.Unique)
})
@JsonPersistable
public final class Flow {
    @com.fasterxml.jackson.annotation.JsonIgnore
    protected transient Logger logger = Logger.getLogger(getClass().getName());
    private FlowId flowId;
    private UserId userId;
    private String name;
    private String description;
    //  @OneToMany
//  @JoinColumn(name = "stone_id", referencedColumnName = "id")
    private Map<UUID, Stone> stones;
    //@CreatedDate
    private long createdAt;
    //@LastModifiedDate
    private long modifiedAt;
    private byte[] signature;

    public Flow() {
        this.createdAt = System.currentTimeMillis();
    }

    public Flow(String name) {
        this.createdAt = System.currentTimeMillis();
        this.name = name;
    }

    public Flow(FlowId flowId, String name, String description) {
        this.flowId = flowId;
        this.name = name;
        this.description = description;
        this.createdAt = System.currentTimeMillis();
    }

    public String getName() {
        return name;
    }

    public void setName(String flowName) {
        this.name = flowName;
        this.modifiedAt = System.currentTimeMillis();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        this.modifiedAt = System.currentTimeMillis();
    }

    public Map<UUID, Stone> getStones() {
        return stones;
    }

    public void setStones(Map<UUID, Stone> stones) {
        this.stones = stones;
        this.modifiedAt = System.currentTimeMillis();
    }

  /*public void pathDfs(Set<String> paths, Stone[] stones, Stone stone, int depth) throws JsonProcessingException {
    stones[depth] = stone;
    for (String destStone : stone.getAllDestStoneNames()) {
      Stone st  = getStoneByName(destStone);
      if (!checkCyclicDependency(stones, st, depth)) {
        pathDfs(paths, stones, st, depth + 1);
      }
    }
    //This is slow as, but whatever its only for printing out.
    //more efficiently would be.
    String p = pathToString(stones, depth);
    for (String path : paths) {
      if (path.contains(p)) {
        return;
      }
    }
    paths.add(pathToString(stones, depth));
  }*/

    public void addStone(Stone stone) {
        if (stones == null) {
            stones = new HashMap<>();
        }
        stones.put(stone.getId(), stone);
        this.modifiedAt = System.currentTimeMillis();
    }

/*    public Graph<Stone, DefaultEdge> toGraph() {
        Graph<Stone, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);

        for (Stone stone : stones) {
            g.addVertex(stone);
        }
        for (Stone stone : stones) {
            g.addEdge(stone, stone.getAllDestStoneNames(st));
        }
        return g;
    }*/

    private String pathToString(Stone[] paths, int depth) {
        String str = "";
        for (int i = 0; i <= depth; i++) {
            if (paths[i] != null) {
                str += paths[i].getName() + " -> ";
                if (i > 0 && i == depth && paths[i].equals(paths[i - 1])) {//cyclic dependency
                    str += "*";
                }
            }
        }
        return str;
    }

  /*public void pathsToString() throws JsonProcessingException {
    Stone[] stones = new Stone[1000]; //1k stones yeah baby
    Set<String> paths = new HashSet<>();
    for (Stone startStone : getStartStones()) {
      pathDfs(paths, stones, startStone, 0);
    }
    for (String path : paths) {
      logger.info("Path: " + path);
    }
  }*/

    private boolean checkCyclicDependency(Stone[] paths, Stone destStone, int depth) {
        for (int i = 0; i < depth; i++) {
            if (paths[i] != null && paths[i].equals(destStone)) {
                return true;
            }
        }
        return false;
    }

    @com.fasterxml.jackson.annotation.JsonIgnore
    public Stone getStoneByName(String name) {
        for (Stone stone : stones.values()) {
            if (stone.getName().equals(name)) {
                return stone;
            }
        }
        throw new IllegalArgumentException("Stone name: " + name + " does not exist");
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(long modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    /**
     * Copies flow.
     *
     * @return copied flow
     */
    public Flow copy(User user) {
        String name = "copy_" + getName();
        Flow flow = new Flow(new FlowId(UUIDProvider.provide(name)), name, description);
        //flow.setCreatedAt(System.currentTimeMillis());
        flow.setUserId(user.getUserId());
        return flow;
    }

    public FlowId getFlowId() {
        return flowId;
    }

    public void setFlowId(FlowId flowId) {
        this.flowId = flowId;
        this.modifiedAt = System.currentTimeMillis();
    }

    public UserId getUserId() {
        return userId;
    }

    public void setUserId(UserId userId) {
        this.userId = userId;
        this.modifiedAt = System.currentTimeMillis();
    }

    public byte[] getSignature() {
        return signature;
    }

    public void setSignature(byte[] flowSignature) {
        this.signature = flowSignature;
    }

    public Stone getStoneById(UUID id) {
        return stones.get(id);
    }

    @Override
    public String toString() {
        try {
            return JsonHelper.toJson(this);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
