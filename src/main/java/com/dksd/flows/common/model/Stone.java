package com.dksd.flows.common.model;

import com.dksd.flows.common.helper.FilterRulesResult;
import com.dksd.flows.common.helper.RoutingRulesResult;
import com.dksd.flows.common.helper.RuleResult;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.teavm.flavour.json.JsonPersistable;

import java.util.*;
import java.util.logging.Logger;

@JsonPersistable
public final class Stone {
    @JsonIgnore
    private final Logger logger = Logger.getLogger(getClass().getName());
    private UUID id;
    private String name;
    private String url;
    private String method;
    private Rule filterRules;
    private Rule routingRules;
    private Rule elseRules;

    public FilterRulesResult applyFilterRules(JsonNode data) {
        return wrapFilterRules(applyRules(filterRules, data));
    }
    private FilterRulesResult wrapFilterRules(RuleResult rule) {
        FilterRulesResult fr = new FilterRulesResult();
        fr.setResult(rule.getResult());
        fr.setDestStones(rule.getDestStoneNames());
        fr.setMatchedIds(rule.getMatchedIds());
        fr.setUnmatchedIdsToErrors(rule.getUnmatchedIdsToErrors());
        return fr;
    }

    public RoutingRulesResult applyRoutingRules(JsonNode data) {
        RuleResult rr = applyRules(routingRules, data);
        if (!rr.getResult()) {
            rr.mergeElseResult(applyRules(elseRules, data));
        }
        return wrapRoutingRules(rr);
    }

    private RoutingRulesResult wrapRoutingRules(RuleResult rule) {
        RoutingRulesResult fr = new RoutingRulesResult();
        fr.setResult(rule.getResult());
        fr.setDestStones(rule.getDestStoneNames());
        fr.setMatchedIds(rule.getMatchedIds());
        fr.setUnmatchedIdsToErrors(rule.getUnmatchedIdsToErrors());
        return fr;
    }
    private RuleResult applyRules(Rule rules, JsonNode data) {
        RuleResult fr = new RuleResult();
        if (rules != null) {
            fr.setResult(evaluate(fr, rules, data));
            fr.setDestStones(rules.getDestStones());
            logger.info("[Stone: apply rules] found filter rule result: " + fr);
        }
        return fr;
    }

    private boolean evaluate(RuleResult fr, Rule rules, JsonNode data) {
        if (!rules.hasChildren()) {
            return apply_rules(fr, rules, data);
        }
        boolean result = false;
        for (Rule child : rules.getChildren()) {
            result = evaluate(fr, child, data);
          if (!result && isAnd(rules)) {
            return false;
          }
          if (result && isOr(rules)) {
            return true;
          }
        }
        return result;
    }

    private boolean apply_rules(RuleResult fr, Rule rule, JsonNode data) {
        try {
            if (rule.isAll()) {
                return fr.addMatched(rule.getId());
            }
            if (rule.getGate() == null) {
                fr.addUnmatched(rule.getId(), "invalid rules: no gate defined for rule " + rule.getId());
                return false;
            }
            Set<String> errors = new HashSet<>();
            boolean result = rule.getGate().filter(data, errors);
            return result ? fr.addMatched(rule.getId()) : fr.addUnmatched(rule.getId(), errors.toString());
        } catch (NullPointerException npe) {
            fr.addUnmatched(rule.getId(), npe.getMessage());
        }
        return false;
    }

    private boolean isAnd(Rule rule) {
        return RuleType.AND.equals(rule.getRuleType());
    }

    private boolean isOr(Rule rule) {
        return RuleType.OR.equals(rule.getRuleType());
    }

    public String getName() {
        return name;
    }

    public void setName(String stoneName) {
        this.name = stoneName;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setFunctionName(String url, String method) {
        this.url = url;
        this.method = method;
    }

    public void addInExampleMessage(String msg) {
        this.addInExampleMessage(msg);
    }

    public void addOutExampleMessage(String msg) {
        this.addOutExampleMessage(msg);
    }

    @JsonIgnore
    public List<String> getAllDestStoneNames(JsonNode rule) {
        if (rule.get("thens") == null) {
            return Collections.singletonList(this.name);
        }
        List<String> destStones = new ArrayList<>();
        ArrayNode thenArr = (ArrayNode) rule.get("thens");
        for (JsonNode jsonNode : thenArr) {
            destStones.add(jsonNode.asText());
        }
        return destStones;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Stone.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .toString();
    }

    public String getUrl() {
        return url;
    }

    public String getMethod() {
        return method;
    }

    public Rule getFilterRules() {
        return filterRules;
    }

    public void setFilterRules(Rule filterRules) {
        this.filterRules = filterRules;
    }

    public Rule getRoutingRules() {
        return routingRules;
    }

    public void setRoutingRules(Rule routingRules) {
        this.routingRules = routingRules;
    }

    public Rule getElseRules() {
        return elseRules;
    }

    public void setElseRules(Rule elseRules) {
        this.elseRules = elseRules;
    }
}

