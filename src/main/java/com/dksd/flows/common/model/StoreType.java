package com.dksd.flows.common.model;

public enum StoreType {
    STATIC, FILE, TOPIC, KV_STORE, KV_STATIC_PAIRS
}
