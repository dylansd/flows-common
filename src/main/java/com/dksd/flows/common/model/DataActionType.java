package com.dksd.flows.common.model;

public enum DataActionType {

    EVENT, DECORATE;

}
