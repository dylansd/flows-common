package com.dksd.flows.common.model;

import com.dksd.flows.common.helper.ExpiringMap;
import com.dksd.flows.common.kafka.TopicDefs;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.teavm.flavour.json.JsonPersistable;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class FlowInfo {

    private static final int RETRY_COUNT = 20;
    private Logger logger = Logger.getLogger(getClass().getName());
    private KafkaProducer<UUID, Tracker> producer;
    //TODO expire to database rather than lost forever
    private ExpiringMap<UUID, Set<Tracker>> idToTracker = new ExpiringMap<>();
    private ExpiringMap<UUID, Set<UUID>> childrenForId = new ExpiringMap<>();
    private ExpiringMap<UUID, Long> totalCharsPerUser = new ExpiringMap<>();

    public FlowInfo(KafkaProducer<UUID, Tracker> producer) {
        this.producer = producer;
    }

    public Set<Tracker> get(UUID id) {
        for (int i = 0; i < RETRY_COUNT; i++) {
            if (idToTracker.containsKey(id)) {
                return idToTracker.get(id);
            }
            ;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return Collections.emptySet();
    }

    public FlowId getFlowId(UUID id) {
        for (Tracker tracker : get(id)) {
            return tracker.getFlowId();
        }
        return null;
    }

    public UserId getUserId(UUID id) {
        for (Tracker tracker : get(id)) {
            return tracker.getUserId();
        }
        return null;
    }

    public void send(UUID id, Tracker tracker) {
        try {
            update(tracker);
            producer.send(new ProducerRecord<>(TopicDefs.TRACKER_TOPIC, id, tracker)).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public Set<UUID> getChildrenFor(UUID id) {
        return childrenForId.get(id);
    }

    public void update(Tracker tracker) {
        idToTracker.computeIfAbsent(tracker.getId(), k -> new HashSet<>());
        idToTracker.get(tracker.getId()).add(tracker);
        if (tracker.getParentId() != null) {
            childrenForId.computeIfAbsent(tracker.getParentId(), k -> new HashSet<>());
            childrenForId.get(tracker.getParentId()).add(tracker.getId());
        }
        UUID userId = tracker.getUserId().getUserId();
        totalCharsPerUser.putIfAbsent(userId, 0L);
        totalCharsPerUser.put(userId, totalCharsPerUser.get(userId) + tracker.getPayloadSizeBytes());
    }

    public long getUserTotalChars(UUID userId) {
        if (!totalCharsPerUser.containsKey(userId)) {
            return 0L;
        }
        return totalCharsPerUser.get(userId);
    }

}
