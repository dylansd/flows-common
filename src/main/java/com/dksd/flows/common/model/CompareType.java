package com.dksd.flows.common.model;

public enum CompareType {
    INT, STRING, DOUBLE
}
