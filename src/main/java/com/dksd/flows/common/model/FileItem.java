package com.dksd.flows.common.model;

public class FileItem {
    private String id;
    private Long lastModified;

    public FileItem(String id, Long lastModified) {
        this.id = id;
        this.lastModified = lastModified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getLastModified() {
        return lastModified;
    }

    public void setLastModified(Long lastModified) {
        this.lastModified = lastModified;
    }
}
