package com.dksd.flows.common.model;

import java.io.Serializable;

public enum RuleType implements Serializable {
    ROUTING, FILTER, ELSE, OR, AND, ALL, SAMPLING
}
