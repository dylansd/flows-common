package com.dksd.flows.common.function.core;

import com.dksd.flows.common.exceptions.IdMisingException;
import com.dksd.flows.common.helper.JsonHelper;
import com.dksd.flows.common.helper.UUIDProvider;
import com.dksd.flows.common.schema.Annotation;
import com.dksd.flows.common.schema.FlowMsgData;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.Serializable;
import java.util.*;

public abstract class AbstractFlowMemFunction implements Serializable {

    private final UUID id;
    private final String functionName = getClass().getName();

    protected AbstractFlowMemFunction() {
        this.id = UUIDProvider.provide("" + functionName.hashCode());
    }

    public abstract List<Annotation> handleMessage(FlowMsgData flowMsg) throws Exception;

    public UUID getId() {
        return id;
    }

    public String getFunctionName() {
        return functionName;
    }

    public JsonNode getParsedPayload(FlowMsgData flowMsg) throws JsonProcessingException {
        return JsonHelper.getParsedPayload(flowMsg);
    }

    public Map<String, String> getMapFromJson(JsonNode node) {
        Map<String, String> map = new HashMap<>();
        while (node.fieldNames().hasNext()) {
            String field = node.fieldNames().next();
            map.put(field, node.get(field).asText());
        }
        return map;
    }

    public UUID getIdFromPayload(FlowMsgData flowMsg) throws JsonProcessingException, IdMisingException {
        JsonNode nd = getParsedPayload(flowMsg);
        if (nd.get("id") != null) {
            return UUID.fromString(nd.get("id").asText());
        }
        List<String> idFields = new ArrayList<>(10);
        nd.fieldNames().forEachRemaining(f -> {
            if (f.contains("id")) {
                idFields.add(f);
            }
        });
        if (idFields.isEmpty()) {
            throw new IdMisingException("AbstractMemFunction: Could not get id from payload: " + flowMsg.getData());
        }
        return UUID.fromString(nd.get(idFields.get(0)).asText());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AbstractFlowMemFunction.class.getSimpleName() + "[", "]")
                .add("functionName='" + functionName + "'")
                .toString();
    }

}
