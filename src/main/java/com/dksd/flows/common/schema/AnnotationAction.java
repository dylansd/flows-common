package com.dksd.flows.common.schema;

public enum AnnotationAction {

    PASS_THRU, CREATE_MSG, ERROR, EMPTY;
}
