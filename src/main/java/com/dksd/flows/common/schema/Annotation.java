package com.dksd.flows.common.schema;

import com.dksd.flows.common.helper.JsonHelper;

import java.util.*;

public final class Annotation {
    private UUID annoId;
    private String annoData;

    public Annotation() {
        this.annoId = UUID.randomUUID();
    }

    public Annotation(String annoData) {
        this.annoId = UUID.randomUUID();
        this.annoData = annoData;
    }

    public static List<Annotation> error(FlowMsgData flowMsg, String msg, Exception ex) {
        return Arrays.asList(new Annotation(msg + ": " + ex));
    }

    public static List<Annotation> ofKV(FlowMsgData fm, String key, String value) {
        return Arrays.asList(new Annotation(JsonHelper.createJson(key, value)));
    }

    public static List<Annotation> ofKV(FlowMsgData fm, String key, String value, String key2, String value2) {
        return Arrays.asList(new Annotation(JsonHelper.createJson(key, value)));
    }

    public static List<Annotation> empty() {
        return Collections.emptyList();
    }

    public static List<Annotation> ofString(FlowMsgData fm, String value) {
        return Arrays.asList(new Annotation(value));
    }

  /*public void addError(UUID originalFlowMsgId, String message, Throwable ep) {
    this(originalFlowMsgId, AnnotationAction.ERROR, message + ep.getMessage());
  }*/

  /*private FlowMsgData createUserFlowMsg(UserId userId, FlowId flowId, String message) {
    //FlowMsgData msg = new FlowMessage(userId, flowId);
    ObjectNode oo = JsonHelper.createWithKeyValue("msg", message);
    oo.put("error_counter", 1);
    msg.setData(JsonHelper.createJson("msg", message));
    return msg;
  }*/

  /*public void addError(FlowMsgData flowMsg, String msg, Throwable e) {
    addError(flowMsg.getUserId(), flowMsg.getFlowId(), msg, e);
  }*/

    public String getAnnoData() {
        return annoData;
    }

    @Override
    public String toString() {
        return "Annotation{" +
                //", stoneName='" + srcStoneName + '\'' +
                ", annotation='" + annoData + '\'' +
                '}';
    }

  /*public String getSrcStoneName() {
    return srcStoneName;
  }

  public void setSrcStoneName(String stoneName) {
    this.srcStoneName = stoneName;
  }*/

    public UUID getAnnoId() {
        return annoId;
    }

    public void setAnnoId(UUID annoId) {
        this.annoId = annoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Annotation that = (Annotation) o;
        return Objects.equals(annoId, that.annoId) && Objects.equals(annoData, that.annoData);
    }

    @Override
    public int hashCode() {
        return Objects.hash(annoId, annoData);
    }
}

