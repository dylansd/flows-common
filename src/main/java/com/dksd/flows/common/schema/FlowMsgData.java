package com.dksd.flows.common.schema;

import java.util.StringJoiner;
import java.util.UUID;

public class FlowMsgData {
    //Would like a notion of a session and
    // run id
    //Imagine I have one flow and want to group
    //different runs through the same flow
    private UUID id;
    private String data;

    public FlowMsgData() {
        this.id = UUID.randomUUID();
    }

    public FlowMsgData(FlowMsgData flowMsgData) {
        this.id = UUID.randomUUID();
        this.data = flowMsgData.data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", FlowMsgData.class.getSimpleName() + "[", "]")
                .add("dataId=" + id)
                .add("data='" + data + "'")
                .toString();
    }
}
